import React from 'react';

import logoNav from '../../assets/logo-nav.png';
import logoNav2 from '../../assets/logo-nav@2x.png';
import logoNav3 from '../../assets/logo-nav@3x.png';
import search from '../../assets/ic-search-copy.svg'


export default function Home(){
    return(
        <div className='body-home'>
            <nav className='nav-menu'>
                <div />
                <img src={logoNav} srcSet={`${logoNav2} 2x, ${logoNav3} 3x`} />
                <img src={search}  className='search'/>
            </nav>
            <div className='content'>
                Clique na busca para iniciar.
            </div>
        </div>
    )
}