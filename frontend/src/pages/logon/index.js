import React, {useState} from 'react';
import {AiFillEye, AiOutlineEye} from 'react-icons/ai';

import '../../styles.css'

import logoHome from '../../assets/logo-home.png'
import logoHome2x from '../../assets/logo-home@2x.png'
import logoHome3x from '../../assets/logo-home@3x.png'
import email from '../../assets/ic-email.svg'
import padlock from '../../assets/ic-cadeado.svg'


export default function Logon(){
    const [view, setView] = useState(true)

    const viewHandle = () =>{
        setView(!view);
    }

    return(
        <div className='_2_LoginSenhaExibida'>
            <div>
                <img 
                    src={logoHome}
                    srcSet={`${logoHome2x} 2x,
                            ${logoHome3x} 3x`}
                    className="logo_home" />
                <p className='BEM-VINDO-AO-EMPRESA'>BEM VINDO AO EMPRESA</p>
                <p className='Lorem-ipsum-dolor-si'>Lorem ipsum dolor sit amet, contetur adpiscing elit. Nunc Accumsan</p>
                <div className='input-row'>
                    <img src={email} className='icon-logon' />
                    <input type='email' className='input-logon' placeholder='abc@yahoo.com'/>
                </div>
                <hr className='Line'/>
                <div className='input-row'>
                    <img src={padlock} className='icon-logon' /> 
                    <input type={`${view ? 'password' : 'name'}`} className='input-logon' placeholder='123456jkh'/>
                    <AiFillEye className={`icon-logon icon-eye ${view ? 'display-none' : '' }`} onClick={viewHandle} />
                    <AiOutlineEye className={`icon-logon icon-eye ${view ? '' : 'display-none'}`} onClick={viewHandle} />
                </div>
                <hr className='Line'/>
                <button  type='submit' className='button-logon'>ENTRAR</button>
            </div>
        </div>
    )
}