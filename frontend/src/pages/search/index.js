import React, {useState, useEffect} from 'react';
import {AiOutlineClose} from 'react-icons/ai'

import search from '../../assets/ic-search-copy.svg'

export default function Search(){
    const [inputValue, setInputValue] = useState('')

    const clearSearch = () =>{
        setInputValue('')
    }
    
    return(
        <div className='body-home'>
            <nav className='nav-menu-search'>
                <div className='input-row'>
                    <img src={search}  className='search2'/>
                    <input type='search' className='input-search' placeholder='Pesquisar' onChange={e=>setInputValue(e.target.value)} value={inputValue} />
                    <AiOutlineClose className={`clear-search-icon ${inputValue==''? 'display-none' : ''}`} onClick={clearSearch} />
                </div>
                <hr className='Line-search'/>
            </nav>
            <div className='content'/>
        </div>
    )
}