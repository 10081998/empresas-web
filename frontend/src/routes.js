import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import Logon from './pages/logon';
import Home from './pages/home';
import Search from './pages/search';

export default function Routes(){
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={Logon}/>
                <Route path='/home' component={Home} />
                <Route path='/search' component={Search} />
            </Switch>
        </BrowserRouter>
        
    )
}